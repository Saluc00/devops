<?php

use PHPUnit\Framework\TestCase;

require_once __DIR__ . '/../vendor/autoload.php'; // Autoload files using Composer autoload
include_once(__DIR__ . "/../Car.php");

class CarTest extends TestCase
{
    public function testGetCarByAlpha()
    {
        $car = new Car();
        $result = $car->getCarLabelById('R');
        $expected = 'Renault';

        $this->assertEquals($result, $expected);
    }

    public function testGetCarByUnknownAlpha()
    {
        $car = new Car();
        $result = $car->getCarIdByLabel('TG');

        $this->assertEmpty($result);
    }
}
