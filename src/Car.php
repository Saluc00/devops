<?php

class Car
{
    /**
     * @var array
     */
    private $cars;

    public function __construct()
    {
        $this->cars = [
            'P' => 'Peugeot',
            'R' => 'Renault',
            'FF' => 'Ferrari'
            // ...
        ];
    }

    public function getCars()
    {
        return $this->cars;
    }

    public function getCarLabelById($id)
    {
        if(empty($id) || !isset($this->cars[$id]))
        {
            return null;
        }

        return $this->cars[$id];
    }

    public function getCarIdByLabel($label)
    {
        if (empty($label)) {
            return null;
        }

        foreach ($this->cars as $id => $clabel)
        {
            if($label === $clabel) {
                return $id;
            }
        }

        return null;
    }
}
